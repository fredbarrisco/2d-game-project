package levelObjectManagers;

import levelObjects.Tile;
import storage_utilities.SaveLoad;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class TileManager {

    public ArrayList<Tile> tilesList = new ArrayList<>();
    public ArrayList<Tile> road = new ArrayList<>();
    public ArrayList<Tile> roadCorners = new ArrayList<>();
    public ArrayList<Tile> waterCorners = new ArrayList<>();
    public ArrayList<Tile> beaches = new ArrayList<>();
    public ArrayList<Tile> islands = new ArrayList<>();

    public BufferedImage imgAtlas;

    public Tile GRASS, WATER, ROAD;

    public TileManager() {
        loadImgAtlas();
        createTiles();
    }

    private void loadImgAtlas() {
        imgAtlas = SaveLoad.getSpriteAtlas();
    }

    private void createTiles() {
        int id = 0;
        tilesList.add(GRASS = new Tile(getSprites(8, 1), id++, "Grass"));
        tilesList.add(WATER = new Tile(getSprites(0, 6), id++, "Water"));
        tilesList.add(ROAD = new Tile(getSprites(9, 0), id++, "Road"));
    }

    public BufferedImage getSprites(int xCord, int yCord) {
        return imgAtlas.getSubimage(xCord * 32, yCord * 32, 32, 32 );
    }

    public BufferedImage getTile(int id) {
        return tilesList.get(id).getSprite();
    }

    public Tile getSelectedTile(int id) {
        return tilesList.get(id);
    }

                //ARRAYLIST GETTERS//
    public ArrayList<Tile> getRoad() {
        return road;
    }

    public ArrayList<Tile> getRoadCorners() {
        return roadCorners;
    }

    public ArrayList<Tile> getWaterCorners() {
        return waterCorners;
    }

    public ArrayList<Tile> getBeaches() {
        return beaches;
    }

    public ArrayList<Tile> getIslands() {
        return islands;
    }
}
