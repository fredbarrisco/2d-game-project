package gameStates;


import java.awt.*;

public interface SceneMethods {

    public void render(Graphics g);

    public void mouseClicked(int xPos, int yPos);

    public void mouseMovedOver(int xPos, int yPos);

    public void mousePressed(int xPos, int yPos);

    public void mouseReleased(int xPos, int yPos);

    public void mouseDragged(int xPos, int yPos);

}