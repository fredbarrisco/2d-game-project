package gameStates;
import main.Game;
import userInterface.MyButton;
import java.awt.*;
import static main.GameStates.*;

public class Menu extends GameScene implements SceneMethods {

    private MyButton playB, editB, settingsB, quitB;

    public Menu(Game game) {
        super(game);
        initButtons();
    }

    private void initButtons() {

        int w = 150;
        int h = w / 3;
        int x = 640 / 2 - w /2;
        int y = 150;
        int yOffSet = 100;

        playB = new MyButton("Play", x, y, w, h);
        editB = new MyButton("Edit", x, y + yOffSet, w, h);
        settingsB = new MyButton("Settings", x, y + yOffSet * 2, w, h);
        quitB = new MyButton("Quit", x, y + yOffSet * 3, w, h);
    }

    @Override
    public void render(Graphics g) {
        drawButtons(g);
    }

    private void drawButtons(Graphics g) {
        playB.drawButton(g);
        editB.drawButton(g);
        settingsB.drawButton(g);
        quitB.drawButton(g);
    }

    @Override
    public void mouseClicked(int xPos, int yPos) {
        if (playB.getBounds().contains(xPos, yPos)) {
            setGameStates(PLAYING);
        }
        if (editB.getBounds().contains(xPos, yPos)) {
            setGameStates(EDITING);
        }
        if (settingsB.getBounds().contains(xPos, yPos)) {
            setGameStates(SETTINGS);
        }
        if (quitB.getBounds().contains(xPos, yPos)) {
            System.exit(0);
        }
    }

    @Override
    public void mouseMovedOver(int xPos, int yPos) {

        playB.setMouseOver(false);
        editB.setMouseOver(false);
        settingsB.setMouseOver(false);
        quitB.setMouseOver(false);

        if (playB.getBounds().contains(xPos, yPos)) {
            playB.setMouseOver(true);
        }
        if (editB.getBounds().contains(xPos, yPos)) {
            editB.setMouseOver(true);
        }
        if (settingsB.getBounds().contains(xPos, yPos)) {
            settingsB.setMouseOver(true);
        }
        if (quitB.getBounds().contains(xPos, yPos)) {
            quitB.setMouseOver(true);
        }
    }

    @Override
    public void mousePressed(int xPos, int yPos) {

        if (playB.getBounds().contains(xPos, yPos)) {
            playB.setMousePressed(true);
        }
        if (editB.getBounds().contains(xPos, yPos)) {
            editB.setMousePressed(true);
        }
        if (settingsB.getBounds().contains(xPos, yPos)) {
            settingsB.setMousePressed(true);
        }
        if (quitB.getBounds().contains(xPos, yPos)) {
            quitB.setMousePressed(true);
        }
    }

    @Override
    public void mouseReleased(int xPos, int yPos) {
        resetButtons();
    }

    @Override
    public void mouseDragged(int xPos, int yPos) {

    }

    private void resetButtons() {
        playB.resetBooleans();
        editB.resetBooleans();
        settingsB.resetBooleans();
        quitB.resetBooleans();
    }
}
