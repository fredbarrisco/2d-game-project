package gameStates;
import main.Game;
import storage_utilities.SaveLoad;
import userInterface.BottomBar;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Playing extends GameScene implements SceneMethods {

    private int[][] lvl;


    private BottomBar bottomBar;
    private int mouseX, mouseY;

    public Playing(Game game) {
        super(game);

        loadDefaultLevel();
        bottomBar = new BottomBar(0, 640, 640, 100, this);
    }

    private void loadDefaultLevel() {
        lvl = SaveLoad.getLevelData("New_level");
    }

    public void saveLevel() {
        SaveLoad.saveLevel("New_level", lvl);
    }

    @Override
    public void render(Graphics g) {

        //Grid of Game Y axis
        for (int y = 0; y < lvl.length; y++) {
            //Grid of Game x axis
            for (int x = 0; x < lvl[y].length; x++) {

                int id = lvl[y][x];
                g.drawImage(getSprite(id), x * 32, y * 32, null);
            }
        }
        bottomBar.draw(g);
    }

    private BufferedImage getSprite(int spriteId) {
        return getGame().getTileManager().getTile(spriteId);
    }

    @Override
    public void mouseClicked(int xPos, int yPos) {
        if (yPos >= 640) {
            bottomBar.buttonClicked(xPos, yPos);
        }
    }

    @Override
    public void mouseMovedOver(int xPos, int yPos) {
        if (yPos >= 640) {
            bottomBar.buttonMovedOver(xPos, yPos);
        } else {
            mouseX = (xPos / 32) * 32;
            mouseY = (yPos / 32) * 32;
        }
    }

    @Override
    public void mousePressed(int xPos, int yPos) {
        if (yPos >= 640) {
            bottomBar.buttonPressed(xPos, yPos);
        } else {
            //CENAS QUE HAVERÂO DE ACONTECER!
        }
    }

    @Override
    public void mouseReleased(int xPos, int yPos) {
        bottomBar.buttonReleased(xPos, yPos);
    }

    @Override
    public void mouseDragged(int xPos, int yPos) {

    }
}

