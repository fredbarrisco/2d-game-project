package gameStates;
import levelObjects.Tile;
import main.Game;
import java.awt.*;

public class Editing extends GameScene implements SceneMethods{

    private int[][] lvl;
    private Tile selectedTile;
    private int mouseX, mouseY;
    private int lastTileX, lastTileY, lastTileId;
    private boolean drawSelect;

    public Editing(Game game) {
        super(game);
    }

    @Override
    public void render(Graphics g) {
        drawSelectedTile(g);
    }

    public void drawSelectedTile(Graphics g) {
        if (selectedTile != null && drawSelect) {
            g.drawImage(selectedTile.getSprite(),mouseX, mouseY, 32, 32, null);
        }
    }

    public void setSelectedTile(Tile tile) {
        this.selectedTile= tile;
        drawSelect = true;
    }

    private void changeTile(int x, int y) {
        if(selectedTile != null) {

            int tileX = x/32;
            int tileY = y/32;

            if(lastTileX == tileX && lastTileY == tileY && lastTileId == selectedTile.getId()) {
                return;
            }
            lastTileX = tileX;
            lastTileY = tileY;

            lvl[tileY][tileX] = selectedTile.getId();
        }
    }

    @Override
    public void mouseClicked(int xPos, int yPos) {
        if (yPos >= 640) {
            bottomBar.buttonClicked(xPos, yPos);
        } else {
            changeTile(mouseX,mouseY);
        }
    }

    @Override
    public void mouseMovedOver(int xPos, int yPos) {
        if (yPos >= 640) {
            bottomBar.buttonMovedOver(xPos, yPos);
            drawSelect = false;
        } else {
            drawSelect = true;
            mouseX = (xPos / 32) * 32;
            mouseY = (yPos / 32) * 32;
        }
    }

    @Override
    public void mousePressed(int xPos, int yPos) {

    }

    @Override
    public void mouseReleased(int xPos, int yPos) {

    }

    @Override
    public void mouseDragged(int xPos, int yPos) {
        if (yPos >= 640) {

        } else {
            changeTile(xPos, yPos);
        }
    }
}
