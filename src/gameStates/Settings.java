package gameStates;

import main.Game;
import userInterface.MyButton;
import java.awt.*;
import static main.GameStates.*;

public class Settings extends GameScene implements SceneMethods {

    private MyButton backB;

    public Settings(Game game) {
        super(game);
        initButtons();
    }

    private void initButtons() {
        backB = new MyButton("Menu", 5, 5, 75, 75/3);
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.blue);
        g.fillRect(0, 0, 640, 640);
        backB.drawButton(g);
    }

    @Override
    public void mouseClicked(int xPos, int yPos) {
        if (backB.getBounds().contains(xPos, yPos)) {
            setGameStates(MENU);
        }
    }


    @Override
    public void mouseMovedOver(int xPos, int yPos) {
        backB.setMouseOver(false);

        if (backB.getBounds().contains(xPos, yPos)) {
            backB.setMouseOver(true);
        }
    }

    @Override
    public void mousePressed(int xPos, int yPos) {
        if (backB.getBounds().contains(xPos, yPos)) {
            backB.setMousePressed(true);
        }
    }

    @Override
    public void mouseReleased(int xPos, int yPos) {
        resetButtons();
    }

    @Override
    public void mouseDragged(int xPos, int yPos) {

    }

    private void resetButtons() {
        backB.resetBooleans();
    }
}
