package userInterface;

import java.awt.*;

public class MyButton {

    public int xPos, yPos, width, height;
    private String buttonText;
    private Rectangle bounds;
    private int id;

    private boolean isMouseOver;
    private boolean isMousePressed;

    //NORMAL BUTTONS
    public MyButton(String buttonText, int xPos, int yPos, int width, int height) {
        this.buttonText = buttonText;
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.id = -1;

        initBounds();
    }

    //TILE BUTTONS
    public MyButton(String buttonText, int xPos, int yPos, int width, int height, int id) {
        this.buttonText = buttonText;
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.id = id;

        initBounds();
    }

    private void initBounds() {
        this.bounds = new Rectangle(xPos, yPos, width, height);
    }

    public void drawButton(Graphics g) {
        drawBody(g);
        drawBorder(g);
        drawText(g);
    }

    private void drawBody(Graphics g) {
        if (isMouseOver) {
            g.setColor(Color.lightGray);
        } else {
            g.setColor(Color.white);
        }
        g.fillRect(xPos, yPos, width, height);
    }

    private void drawBorder(Graphics g) {
        g.setColor(Color.BLACK);
        g.drawRect(xPos, yPos, width, height);

        if (isMousePressed) {
            g.drawRect(xPos + 1, yPos + 1, width - 2, height - 2);
            g.drawRect(xPos + 2, yPos + 2, width - 4, height - 4);
        }
    }

    private void drawText(Graphics g) {
        int w = g.getFontMetrics().stringWidth(buttonText);
        int h = g.getFontMetrics().getHeight();
        g.drawString(buttonText,xPos - w / 2 + width / 2, yPos + h / 2 + height / 2);
    }

    public void resetBooleans() {
        this.isMouseOver = false;
        this.isMousePressed = false;
    }

    public void setMouseOver(boolean isMouseOver) {
        this.isMouseOver = isMouseOver;
    }
    public boolean isMouseOver() {
        return isMouseOver;
    }

    public void setMousePressed(boolean isMousePressed) {
        this.isMousePressed = isMousePressed;
    }
    public boolean isMousePressed() {
        return isMousePressed;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public int getId() {
        return id;
    }
}
