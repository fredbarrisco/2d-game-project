package userInterface;

import gameStates.Playing;
import levelObjects.Tile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import static main.GameStates.MENU;
import static main.GameStates.setGameStates;

public class BottomBar {

    private int xPos, yPos, width, height;
    private MyButton menuB, saveB;
    private Playing playing;

    private ArrayList<MyButton> tileButtons = new ArrayList<>();
    private Tile selectedTile;

    public BottomBar(int xPos, int yPos, int width, int height, Playing playing) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
        this.playing = playing;

        initButtons();
    }

    private void initButtons() {

        menuB = new MyButton("Menu", 2, 642, 100, 30);
        saveB = new MyButton("Save", 2, 674, 100, 30);

        int w = 50;
        int h = 50;
        int xStart = 110;
        int yStart = 650;
        int xOfSet = (int) (w * 1.1f);
        int i = 0;

        for (Tile tile : playing.getTileManager().tilesList) {
            tileButtons.add(new MyButton(tile.getName(), xStart + xOfSet * i, yStart, w, h, i));
            i++;
        }
    }

    public void draw(Graphics g) {
        //BACKGROUND
        g.setColor(new Color(220, 125, 15));
        g.fillRect(xPos, yPos, width, height);

        //BUTTONS
        drawButtons(g);
        drawTileButtons(g);
        drawSelectedTile(g);
    }

    public void drawButtons(Graphics g) {
        menuB.drawButton(g);
        saveB.drawButton(g);
    }

    private void drawTileButtons(Graphics g) {

        for (MyButton b : tileButtons) {

            //Sprites (Images)
            g.drawImage(getButtonImg(b.getId()), b.xPos, b.yPos, b.width, b.height, null);

            //Mouse Over
            if (b.isMouseOver()) {
                g.setColor(Color.WHITE);
            } else {
                g.setColor(Color.black);
            }

            //Border
            g.drawRect(b.xPos, b.yPos, b.width, b.height);

            //Mouse Pressed
            if (b.isMousePressed()) {
                g.drawRect(b.xPos + 1, b.yPos + 1, b.width - 2, b.height - 2);
                g.drawRect(b.xPos + 2, b.yPos + 2, b.width - 4, b.height - 4);

            }
        }
    }

    private void drawSelectedTile(Graphics g) {
        if (selectedTile != null) {
            g.drawImage(selectedTile.getSprite(), 550, 650, 50, 50, null);
            g.setColor(Color.black);
            g.drawRect(550, 650, 50, 50);
        }
    }

    private BufferedImage getButtonImg(int id) {
        return playing.getTileManager().getTile(id);
    }

    private void saveLevel() {
        playing.saveLevel();
    }

    public void buttonClicked(int xPos, int yPos) {

        if (menuB.getBounds().contains(xPos, yPos)) {
            setGameStates(MENU);
        } else if (saveB.getBounds().contains(xPos, yPos)) {
            saveLevel();

        } else {
            for (MyButton b : tileButtons) {
                if (b.getBounds().contains(xPos, yPos)) {
                    selectedTile = playing.getTileManager().getSelectedTile(b.getId());
                    playing.setSelectedTile(selectedTile);
                    return;
                }
            }
        }
    }

    public void buttonMovedOver(int xPos, int yPos) {
        menuB.setMouseOver(false);
        saveB.setMouseOver(false);
        for (MyButton b : tileButtons) {
            b.setMouseOver(false);
        }

        if (menuB.getBounds().contains(xPos, yPos)) {
            menuB.setMouseOver(true);
        } else if (saveB.getBounds().contains(xPos, yPos)) {
            saveB.setMouseOver(true);
        } else {
            for (MyButton b : tileButtons) {
                if (b.getBounds().contains(xPos, yPos)) {
                    b.setMouseOver(true);
                    return;
                }
            }
        }
    }

    public void buttonPressed(int xPos, int yPos) {
        if (menuB.getBounds().contains(xPos, yPos)) {
            menuB.setMousePressed(true);
        } else if (saveB.getBounds().contains(xPos, yPos)) {
            saveB.setMousePressed(true);
        } else {
            for (MyButton b : tileButtons) {
                if (b.getBounds().contains(xPos, yPos)) {
                    b.setMousePressed(true);
                    return;
                }
            }
        }
    }

    public void buttonReleased(int xPos, int yPos) {
        menuB.resetBooleans();
        saveB.resetBooleans();
        for (MyButton b : tileButtons) {
            b.resetBooleans();
        }
    }
}

