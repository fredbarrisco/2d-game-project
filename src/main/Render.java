package main;

import java.awt.*;

public class Render {

    private Game game;

    Render(Game game) {
        this.game = game;
    }

    public void renderScreen(Graphics g) {

        switch (GameStates.gameStates) {
            case MENU:
                game.getMenu().render(g);
                break;

            case PLAYING:
                game.getPlaying().render(g);
                break;

            case SETTINGS:
                game.getSettings().render(g);
                break;

            case EDITING:
                game.getEditor().render(g);
                break;
        }
    }
}
