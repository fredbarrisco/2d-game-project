package main;

import inputs.KeyboardListener;
import inputs.MouseListener;

import javax.swing.*;
import java.awt.*;

public class GameScreen extends JPanel {

    private MouseListener myMouse;
    private KeyboardListener myKeyboard;
    private Dimension size;
    private Game game;

    GameScreen(Game game) {
        this.game = game;
        setScreenSize();
    }

    public void initInputs() {
        myMouse = new MouseListener(game);
        addMouseListener(myMouse);
        addMouseMotionListener(myMouse);

        myKeyboard = new KeyboardListener();
        addKeyListener(myKeyboard);

        //Focus on all JPanel given inputs
        requestFocus();
    }

    private void setScreenSize() {
        size = new Dimension(640, 740);
        setMaximumSize(size);
        setMinimumSize(size);
        setPreferredSize(size);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        game.getRender().renderScreen(g);
    }
}
