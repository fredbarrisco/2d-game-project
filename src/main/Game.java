package main;

import gameStates.Editing;
import inputs.KeyboardListener;
import inputs.MouseListener;
import gameStates.Menu;
import gameStates.Playing;
import gameStates.Settings;
import levelObjectManagers.TileManager;
import storage_utilities.SaveLoad;

import javax.swing.*;

public class Game extends JFrame implements Runnable {

    private final double FPS_SET = 120.0;
    private final double UPS_SET = 60.0;

    private int updates;
    private long lastTimeUPS;
    private Thread gameThread;

    //Classes
    private GameScreen gameScreen;
    private Render render;
    private Menu menu;
    private Playing playing;
    private Settings settings;
    private Editing editing;
    private TileManager tileManager;


    public static void main(String[] args) {
        Game myGame = new Game();
        myGame.gameScreen.initInputs();
        myGame.start();
    }

    Game() {
        gameScreen = new GameScreen(this);
        add(gameScreen);
        pack();
        initClasses();

        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void initClasses() {
        createDefaultLevel();

        render = new Render(this);
        menu = new Menu(this);
        playing = new Playing(this);
        settings = new Settings(this);
        editing = new Editing(this);
        tileManager = new TileManager();
    }

    private void createDefaultLevel() {

        int[] arr = new int[400];

        for(int i = 0 ; i<arr.length ; i++) {
            arr[i] = 0;
        }
        SaveLoad.createLevel("New_level", arr);
    }

    private void start() {
        gameThread = new Thread(this);
        gameThread.start();
    }

    private void callUPS() {
        if (System.currentTimeMillis() - lastTimeUPS >= 1000) {
            System.out.println("UPS: " + updates);
            updates = 0;
            lastTimeUPS = System.currentTimeMillis();
        }
    }

    private void updateGame() {

    }

    @Override
    public void run() {

        long lastTimeChecked = System.currentTimeMillis();
        int frames = 0;
        int updates = 0;

        double timePerFrame;
        double timePerUpdate;

        timePerFrame = 1000000000.0 / FPS_SET;
        timePerUpdate = 1000000000.0 / UPS_SET;

        long lastFrame = System.nanoTime();
        long lastUpdate = System.nanoTime();
        long now;


        while (true) {
            now = System.nanoTime();

            //Render
            if (now - lastFrame >= timePerFrame) {
                repaint();
                lastFrame = now;
                frames++;
            }
            //Update
            if (now - lastUpdate >= timePerUpdate) {
                updateGame();
                lastUpdate = now;
                updates++;
            }
            //Check FPS / UPS status
            if (System.currentTimeMillis() - lastTimeChecked >= 1000) {
                System.out.println("FPS:" + frames + "\n" + "UPS:" + updates);
                frames = 0;
                updates = 0;
                lastTimeChecked = System.currentTimeMillis();
            }
        }
    }

                        //Getters//
    //Render Main Class
    public Render getRender() {
        return render;
    }

    //Menu Scene Class
    public Menu getMenu() {
        return menu;
    }

    //Playing Scene Class
    public Playing getPlaying() {
        return playing;
    }

    //Settings Scene Class
    public Settings getSettings() {
        return settings;
    }

    //Editing scene Class
    public Editing getEditor() {
        return editing;
    }

    //Tile Manager Class
    public TileManager getTileManager() {
        return tileManager;
    }
}