package main;

public enum GameStates {

    PLAYING,
    MENU,
    SETTINGS,
    EDITING;

    public static GameStates gameStates = MENU;

    public static void setGameStates(GameStates state) {
        gameStates = state;
    }
}
