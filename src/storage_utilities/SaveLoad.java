package storage_utilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class SaveLoad {

    public static BufferedImage getSpriteAtlas() {
        BufferedImage img = null;

        InputStream atlas = SaveLoad.class.getClassLoader().getResourceAsStream("spriteatlas.png");
        try {
            img = ImageIO.read(atlas);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

    public static void createFile(){
        File txtFile = new File("res/testText.txt");
        try {
            txtFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("File creation failed!");
        }
    }

    public static void createLevel(String name, int[] idArray) {

        File newLevel = new File("res/" + name + ".txt");

        if(newLevel.exists()) {
            System.out.println("File " + name + " already exist!");
            return;
        } else {
            try {
                newLevel.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Lvl file creation failed.");
            }
            writeToFile(newLevel, idArray);
        }
    }

    private static void writeToFile(File f, int[] idArray) {
        try {
            PrintWriter pw = new PrintWriter(f);

            for (Integer i : idArray) {
                pw.println(i);
            }

            pw.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Writing to file failed!");
        }
    }

    public static void saveLevel(String name, int[][] idArr) {
        File levelFile = new File("res/" + name + ".txt");

        if (levelFile.exists()) {
            writeToFile(levelFile, Utilities.twoDto1DArray(idArr));
        } else {
            System.out.println("File " + name + " does not exist!");
            return;
        }
    }

    private static ArrayList<Integer> readFromFile(File file) {

        ArrayList<Integer> list = new ArrayList<>();

        try {
            Scanner sc = new Scanner(file);

            while(sc.hasNextLine()) {
                list.add(Integer.parseInt(sc.nextLine()));
            }
            sc.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static int[][] getLevelData(String name ){
        File lvlFile = new File("res/" + name + ".txt");

        if (lvlFile.exists()) {
            ArrayList<Integer> list = readFromFile(lvlFile);
            return Utilities.ArraylistTooInt(list, 20, 20);

        } else {
            System.out.println("File " + name + " does not exist!");
            return null;
        }
    }
}
