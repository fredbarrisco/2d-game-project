package storage_utilities;

import java.util.ArrayList;

public class Utilities {

    public static int[][] ArraylistTooInt(ArrayList<Integer> list, int ySize, int xSize){

        int[][] newArr = new int[ySize][xSize];

        for (int j = 0; j < newArr.length; j++) {
            for (int i = 0; i < newArr[j].length; i++) {
                int index = j * ySize + i;
                newArr[j][i] = list.get(index);
            }
        }
        return newArr;
    }

    public static int[] twoDto1DArray(int [][] TwoDArray) {

        int[] oneDArray = new int[TwoDArray.length * TwoDArray[0].length];

        for (int j = 0; j < TwoDArray.length; j++) {
            for (int i = 0; i < TwoDArray[j].length; i++) {
                int index = j * TwoDArray.length + i;
                oneDArray[index] = TwoDArray[j][i];
            }
        }
    return oneDArray;
    }
}
